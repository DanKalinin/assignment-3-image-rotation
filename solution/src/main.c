#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "bmp.h"
#include "files.h"
#include "rotate.h"

int main(int argc, char** argv) {
	// Check count of args	
	if (argc != 3) {
		fprintf(stderr, "Arguments count error\n");
    	return 0;
	}

	// Open source file
	FILE *source_file = file_open(argv[1],"rb");
	if (!source_file){
		fprintf(stderr, "Open souce file error\n");
    	return 0;
	}

	// Get source image from file
	struct image *source_img = from_bmp(source_file);
	if (!source_img){
		fprintf(stderr, "Get image from source file error\n");
    	return 0;
	}

	// Close source file
	if (file_close(source_file) != 0) {
		fprintf(stderr, "Close source file error\n");
	}

	// Transform source image
	struct image *transformed_img = rotate(source_img);
	if (!transformed_img){
		fprintf(stderr, "Transform image error\n");
		
		// Free source image
		image_free(source_img);
		
    	return 0;
	}

	// Free source image
	image_free(source_img);
	
	//Create file from transformed image
	FILE *transformed_file = file_open(argv[2],"wb");
	if (!transformed_file){
		fprintf(stderr, "Open transformed file error\n");

		// Close transformed file
		if (file_close(transformed_file) != 0) {
			fprintf(stderr, "Close transformed file error\n");
		}

		return 0;
	}

	if (to_bmp(transformed_file, transformed_img) != 0){
		fprintf(stderr, "Create file from transformed image error\n");

		// Close transformed file
		if (file_close(transformed_file) != 0) {
			fprintf(stderr, "Close transformed file error\n");
		}

		// Free transformed image
		image_free(transformed_img);

    	return 0;
	}
	
	// Close transformed file
	if (file_close(transformed_file) != 0) {
		fprintf(stderr, "Close transformed file error\n");
		
		// Free transformed image
		image_free(transformed_img);
		
    	return 0;
	}

	// Free transformed image
	image_free(transformed_img);
	return 0;
}
