#include <stdint.h>
#include <stdio.h>

#include "image.h"
#include "rotate.h"

struct image* rotate(struct image* const source_img){
	if (!source_img) return NULL;

	struct image* transformed_img = image_create(source_img->height, source_img->width);
	if (!transformed_img) return NULL;	
	
	
	struct pixel* source_data = source_img->data;
	struct pixel* transformed_data = transformed_img->data;
	

	for (uint64_t x = 0; x < source_img->width; x++) {
        for (uint64_t y = 0; y < source_img->height; y++) {
            transformed_data[x * transformed_img->width + y] = source_data[(transformed_img->width - y - 1)*transformed_img->height + x];
        }
    }

	return transformed_img;

}

