#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "bmp.h"

struct bmp_header bmp_header_from_image(struct image* const img){
	return (struct bmp_header){
        .bfType = BMP_TYPE,
        .bfileSize = sizeof(struct bmp_header) + image_size(img),
        .bfReserved = BMP_RESERVED,
        .bOffBits = BMP_OFF_BITS,
        .biSize = BMP_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = BMP_PLANES,
        .biBitCount = BMP_BIT_COUNT,
        .biCompression = BMP_COMPRESSION,
        .biSizeImage = image_size(img),
        .biXPelsPerMeter = BMP_X_PELS_PER_METER,
        .biYPelsPerMeter = BMP_Y_PELS_PER_METER,
        .biClrUsed = BMP_CLR_USED,
        .biClrImportant = BMP_CLR_IMPORTANT
    };
}

void header_print(struct bmp_header* const header){
	      printf("bfType = %d\n", header->bfType);
        printf("bfileSize = %d\n", header->bfileSize);
        printf("bbfReservedfType = %d\n", header->bfReserved);
        printf("bOffBits = %d\n", header->bOffBits);
        printf("biSize = %d\n", header->biSize);
        printf("biWidth = %d\n", header->biWidth);
        printf("biHeight = %d\n", header->biHeight);
        printf("biPlanes = %d\n", header->biPlanes);
        printf("biBitCount = %d\n", header->biBitCount);
        printf("biCompression = %d\n", header->biCompression);
        printf("biSizeImage = %d\n", header->biSizeImage);
        printf("biXPelsPerMeter = %d\n", header->biXPelsPerMeter);
        printf("biYPelsPerMeter = %d\n", header->biYPelsPerMeter);
        printf("biClrUsed = %d\n", header->biClrUsed);
        printf("biClrImportant = %d\n", header->biClrImportant);
}

struct image* from_bmp(FILE* const in){
	struct bmp_header header = {0};
	
	if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
		return NULL;
	
	//header_print(&header);
	
	if (header.bfType != BMP_TYPE)
		return NULL;

  	if (header.biBitCount != 24)
		return NULL;

  	if (header.biSize != BMP_SIZE)
		return NULL;

	struct image* img = image_create(header.biWidth, header.biHeight);
	if (!img)
		return NULL;
	
	if (fseek(in, header.bOffBits, SEEK_SET) != 0){
		image_free(img);
		return NULL;
	}

	uint64_t padding = image_padding(img);

  	for (size_t i = 0; i < header.biHeight; i++){
		if (fread(img->data + i*header.biWidth, sizeof(struct pixel), header.biWidth, in) != header.biWidth){
			image_free(img);
			return NULL;
		}

		if (fseek(in, (long) padding, SEEK_CUR) != 0){
			image_free(img);
			return NULL;
		};
	}

	return img;
}



enum write_status to_bmp( FILE* const out, struct image* const img ){
	struct bmp_header header = bmp_header_from_image(img);
	uint64_t padding = image_padding(img);
	
	//header_print(&header);
	
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
		return WRITE_HEADER_ERROR;
	
	if (fseek(out, header.bOffBits, SEEK_SET) != 0)
			return WRITE_HEADER_ERROR;

	for(size_t i = 0; i < header.biHeight; i++){
		if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width)
			return WRITE_ROW_ERROR;
		
		uint32_t tmp = 0;
		if (fwrite(&tmp, padding, 1, out) != 1)			
			return WRITE_PADDING_ERROR;
	}

  return WRITE_OK;
}

