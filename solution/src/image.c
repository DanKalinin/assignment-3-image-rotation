#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

struct image* image_create(const uint64_t width, const uint64_t height){
	size_t row_size = width * sizeof(struct pixel);
	uint64_t padding = 0;

	if (row_size % 4 != 0)
		padding += (4 - row_size % 4);

	struct pixel *tmp = malloc((row_size + padding) * height);
	if (!tmp){
		free(tmp);		
		return NULL;
	}

  	struct image *img = malloc(sizeof(struct image));
	if (!img){
		free(tmp);
		free(img);	
		return NULL;
	}

  	img->data = tmp;
  	img->width = width;
 	img->height = height;
	
  	return img;
}



void image_free(struct image *img) {
	if (!img) return;
	free( img->data );
	free( img );
}



size_t image_padding(struct image* const img){
	size_t row_size = img->width * sizeof(struct pixel);
	uint64_t padding = 0;

	if (row_size % 4 != 0)
		padding += (4 - row_size % 4);

	return padding;
}



uint64_t image_width(struct image* const img){
	return img->width;
}



uint64_t image_height(struct image* const img){
	return img->height;
}



uint32_t image_size(struct image* const img){
	return (uint32_t) (img->width*sizeof(struct pixel) + image_padding(img)) * img->height;
}



