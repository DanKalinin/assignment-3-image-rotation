#include <stdio.h>

#include "files.h"

FILE* file_open(char* const filename, char* const mode){
	FILE* tmp = NULL;
	tmp = fopen(filename, mode);
	if (!tmp)
		fprintf(stderr, "cannot open file \n");
	return tmp;
}


enum close_status file_close(FILE* file){
	// Error
	if (fclose(file) != 0) return CLOSE_ERROR;
	
	//Correct
	return CLOSE_OK;
}
