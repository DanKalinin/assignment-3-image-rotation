#pragma once
#ifndef _ROTATE_H_
#define _ROTATE_H_

struct image* rotate(struct image* const source_img);

#endif
