#pragma once
#ifndef _BMP_H_
#define _BMP_H_

#define BMP_TYPE 0x4D42
#define BMP_RESERVED 0
#define BMP_OFF_BITS 54
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_X_PELS_PER_METER 0
#define BMP_Y_PELS_PER_METER 0
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0

#include <stdint.h>

struct __attribute__((packed)) bmp_header{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};

/*  deserializer   */
//Изначально хотел сделать с форматом enam read_status image *from_bmp( FILE* in, struct image** img), но конструкция получилась слишком монструозной и нецелесообразной ради сохранения read_status
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_CANT_READ_HEADER,
	READ_CANT_CREATE_IMAGE,
	READ_CANT_SEEK_END_OF_HEADER,
	READ_INVALID_ROW,
	READ_INVALID_PADDING
  };

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_HEADER_ERROR,
	WRITE_ROW_ERROR,
	WRITE_PADDING_ERROR
};

void header_print(struct bmp_header* const header);

struct bmp_header from_image(struct image* const img);
struct image *from_bmp( FILE* const in);
enum write_status to_bmp( FILE* const out, struct image* const img );

#endif

