#pragma once
#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdint.h>

struct __attribute__((packed)) pixel {
	uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image* image_create(const uint64_t width, const uint64_t height);
void image_free(struct image *img);
size_t image_padding(struct image* const img);
uint64_t image_width(struct image* const img);
uint64_t image_height(struct image* const img);
uint32_t image_size(struct image* const img);

#endif
