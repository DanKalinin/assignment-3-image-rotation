#pragma once
#ifndef _FILES_H_
#define _FILES_H_

/* close file status */
enum  close_status  {
  CLOSE_OK = 0,
	CLOSE_ERROR
};

FILE* file_open(char* const filename, char* const mode);
enum close_status file_close(FILE* file);

#endif
